#include <ESP8266WiFi.h>
#include <DHT.h>;

#define DHTPIN 4 //pin 4 is actually D2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

const char* ssid = "enatoskinezos";
const char* password = "12345677";

const char* host = "dweet.io";
const int port = 80;

const int sleepTimeSeconds = 300;

float hum;
float temp;

WiFiClient client;

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(2000);
  Serial.println();
  Serial.print("Initializing serial ");
  while(!Serial) {
    Serial.print(".");
    delay(1000);
  }
  dht.begin();
  WiFi.begin(ssid, password);
  
  Serial.println();
  
  Serial.print("Connecting to WiFi ");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.println("WiFi connected");

  if (!client.connect(host, port)) {
    Serial.print("Connecting to ");
    Serial.print(host);
    Serial.print(" failed");
    Serial.println();
    return;
  }
  
  hum = dht.readHumidity();
  temp = dht.readTemperature();

  Serial.print("Temperature: ");
  Serial.print(temp);
  Serial.println("C");
  Serial.print("Humidity: ");
  Serial.print(hum);
  Serial.println("%");
  Serial.println();
  Serial.println();

  client.print(String("GET /dweet/for/inherent-nodemcu?temp=" + String(temp) + "&humidity="+ String(hum) + " HTTP/1.1\r\n" + "Host: " + String(host) + "\r\n" + "Connection: close\r\n\r\n"));
  delay(10);

  while(client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("Going into deep sleep mode");
  ESP.deepSleep(sleepTimeSeconds * 1000000);
}

void loop() {}
